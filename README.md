1) qemu-system-x86_64 -nographic -bios u-boot.rom -kernel bzImage -append 'root=/dev/ram console=ttyS0' -initrd initrd.img -m 512M  
2) qfw load  
3) zboot 01000000 - 04000000 [initram size]  
  
test-img.raw - buildroot made image with grub2 bootloader.  
To start it use:  
$ cd path/to/test-img.raw  
$ qemu-system-x86_64 -hda test-img.raw  
  
user: root  
passwd: 1234  
  
To use this image with VirtualBox it's necessary to convert it to VDI format:  
$ vboxmanage convertdd test-img.raw test-img.vdi --format VDI  
